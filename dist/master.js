"use strict";

var AdditiveSynth = function(context) {
  this.ctx = context;
  // Initialize all of the nodes of the synth
  this.osc1 = this.ctx.createOscillator();
  this.osc2 = this.ctx.createOscillator();
  this.osc3 = this.ctx.createOscillator();
  this.gain1 = this.ctx.createGain();
  this.gain2 = this.ctx.createGain();
  this.gain3 = this.ctx.createGain();
  this.output = this.ctx.createGain();

  // Set initial pitch and oscillator partials
  this.baseFrequency = 220;
  this.partial1 = 1;
  this.partial2 = 1.5;
  this.partial3 = 2;

  // Set initial wave types and frequencies
  this.osc1.type = "sine";
  this.osc1.frequency.value = this.baseFrequency * this.partial1;
  this.osc2.type = "sine";
  this.osc2.frequency.value = this.baseFrequency * this.partial2;
  this.osc3.type = "sine";
  this.osc3.frequency.value = this.baseFrequency * this.partial3;

  // Connect oscillators to gain to control band amplitude
  this.osc1.connect(this.gain1);
  this.osc2.connect(this.gain2);
  this.osc3.connect(this.gain3);

  // Set initial band output amplitudes
  this.gain1.gain.value = 0.33;
  this.gain2.gain.value = 0.33;
  this.gain3.gain.value = 0.33;

  // Connect each band to the synth output
  this.gain1.connect(this.output);
  this.gain2.connect(this.output);
  this.gain3.connect(this.output);

  // Connect the synth output to the Audio Context output destination and silence synth output until triggered
  this.output.gain.value = 0.0;
  this.output.gain.linearRampToValueAtTime(0.0, this.ctx.currentTime + 0.1);
  this.output.connect(this.ctx.destination);

  // Start the oscillators to initialize synth audio stream
  this.osc1.start();
  this.osc2.start();
  this.osc3.start();
}

AdditiveSynth.prototype.fadeIn = function() {
  this.output.gain.value = 0;
  this.output.gain.linearRampToValueAtTime(1.0, this.ctx.currentTime + 0.5);
}

AdditiveSynth.prototype.fadeOut = function() {
  this.output.gain.linearRampToValueAtTime(0.0, this.ctx.currentTime + 0.5);
}

var ctx = new(window.AudioContext || window.webkitAudioContext)();

var synth = new AdditiveSynth(ctx);

function syncView() {
  document.querySelector(".wave-type-select-1").value = synth.osc1.type;
  document.querySelector(".wave-type-select-2").value = synth.osc2.type;
  document.querySelector(".wave-type-select-3").value = synth.osc3.type;
  document.querySelector(".partial-select-1").value = synth.partial1;
  document.querySelector(".partial-select-2").value = synth.partial2;
  document.querySelector(".partial-select-3").value = synth.partial3;
  document.querySelector(".gain-select-1").value = synth.gain1;
  document.querySelector(".gain-select-2").value = synth.gain2;
  document.querySelector(".gain-select-3").value = synth.gain3;
}

function handleWaveTypeSettings(el) {
  var id = el.className.slice(-1);
  var val = el.value;
  switch (id) {
    case "1":
      synth.osc1.type = val;
      break;
    case "2":
      synth.osc2.type = val;
      break;
    case "3":
      synth.osc3.type = val;
      break;
  }
}

function handleGainSettings(el) {
  var id = el.className.slice(-1);
  var val = el.value;
  switch (id) {
    case "1":
      synth.gain1.gain.value = val/100;
      break;
    case "2":
      synth.gain2.gain.value = val/100;
      break;
    case "3":
      synth.gain3.gain.value = val/100;
      break;
  }
}

function playNote(frequency) {
  synth.baseFrequency = frequency;
  synth.osc1.frequency.value = synth.baseFrequency * synth.partial1;
  synth.osc2.frequency.value = synth.baseFrequency * synth.partial2;
  synth.osc3.frequency.value = synth.baseFrequency * synth.partial3;
  synth.fadeIn();
}

function stopNote() {
  synth.fadeOut();
}

var keys = document.querySelectorAll(".keyboard .keys .key");
keys.forEach(function(el) {
  el.addEventListener("mousedown", function() {
    playNote(el.dataset.pitch);
  });
  el.addEventListener("mouseup", function() {
    stopNote();
  });
});

function handlePartialSettings(el) {
  var id = el.className.slice(-1);
  var val = el.value;
  switch (id) {
    case "1":
      synth.partial1 = val;
      synth.osc1.frequency.value = synth.baseFrequency * synth.partial1;
      break;
    case "2":
      synth.partial2 = val;
      synth.osc1.frequency.value = synth.baseFrequency * synth.partial2;
      break;
    case "3":
      synth.partial3 = val;
      synth.osc1.frequency.value = synth.baseFrequency * synth.partial3;
      break;
  }
}

window.onload = syncView();
