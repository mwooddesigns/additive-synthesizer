var AdditiveSynth = function(context) {
  this.ctx = context;
  // Initialize all of the nodes of the synth
  this.osc1 = this.ctx.createOscillator();
  this.osc2 = this.ctx.createOscillator();
  this.osc3 = this.ctx.createOscillator();
  this.gain1 = this.ctx.createGain();
  this.gain2 = this.ctx.createGain();
  this.gain3 = this.ctx.createGain();
  this.output = this.ctx.createGain();

  // Set initial pitch and oscillator partials
  this.baseFrequency = 220;
  this.partial1 = 1;
  this.partial2 = 1.5;
  this.partial3 = 2;

  // Set initial wave types and frequencies
  this.osc1.type = "sine";
  this.osc1.frequency.value = this.baseFrequency * this.partial1;
  this.osc2.type = "sine";
  this.osc2.frequency.value = this.baseFrequency * this.partial2;
  this.osc3.type = "sine";
  this.osc3.frequency.value = this.baseFrequency * this.partial3;

  // Connect oscillators to gain to control band amplitude
  this.osc1.connect(this.gain1);
  this.osc2.connect(this.gain2);
  this.osc3.connect(this.gain3);

  // Set initial band output amplitudes
  this.gain1.gain.value = 0.33;
  this.gain2.gain.value = 0.33;
  this.gain3.gain.value = 0.33;

  // Connect each band to the synth output
  this.gain1.connect(this.output);
  this.gain2.connect(this.output);
  this.gain3.connect(this.output);

  // Connect the synth output to the Audio Context output destination and silence synth output until triggered
  this.output.gain.value = 0.0;
  this.output.gain.linearRampToValueAtTime(0.0, this.ctx.currentTime + 0.1);
  this.output.connect(this.ctx.destination);

  // Start the oscillators to initialize synth audio stream
  this.osc1.start();
  this.osc2.start();
  this.osc3.start();
}

AdditiveSynth.prototype.fadeIn = function() {
  this.output.gain.value = 0;
  this.output.gain.linearRampToValueAtTime(1.0, this.ctx.currentTime + 0.5);
}

AdditiveSynth.prototype.fadeOut = function() {
  this.output.gain.linearRampToValueAtTime(0.0, this.ctx.currentTime + 0.5);
}
