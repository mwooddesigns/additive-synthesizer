var ctx = new(window.AudioContext || window.webkitAudioContext)();

var synth = new AdditiveSynth(ctx);

function syncView() {
  document.querySelector(".wave-type-select-1").value = synth.osc1.type;
  document.querySelector(".wave-type-select-2").value = synth.osc2.type;
  document.querySelector(".wave-type-select-3").value = synth.osc3.type;
  document.querySelector(".partial-select-1").value = synth.partial1;
  document.querySelector(".partial-select-2").value = synth.partial2;
  document.querySelector(".partial-select-3").value = synth.partial3;
  document.querySelector(".gain-select-1").value = synth.gain1;
  document.querySelector(".gain-select-2").value = synth.gain2;
  document.querySelector(".gain-select-3").value = synth.gain3;
}

function handleWaveTypeSettings(el) {
  var id = el.className.slice(-1);
  var val = el.value;
  switch (id) {
    case "1":
      synth.osc1.type = val;
      break;
    case "2":
      synth.osc2.type = val;
      break;
    case "3":
      synth.osc3.type = val;
      break;
  }
}

function handleGainSettings(el) {
  var id = el.className.slice(-1);
  var val = el.value;
  switch (id) {
    case "1":
      synth.gain1.gain.value = val/100;
      break;
    case "2":
      synth.gain2.gain.value = val/100;
      break;
    case "3":
      synth.gain3.gain.value = val/100;
      break;
  }
}

function playNote(frequency) {
  synth.baseFrequency = frequency;
  synth.osc1.frequency.value = synth.baseFrequency * synth.partial1;
  synth.osc2.frequency.value = synth.baseFrequency * synth.partial2;
  synth.osc3.frequency.value = synth.baseFrequency * synth.partial3;
  synth.fadeIn();
}

function stopNote() {
  synth.fadeOut();
}

var keys = document.querySelectorAll(".keyboard .keys .key");
keys.forEach(function(el) {
  el.addEventListener("mousedown", function() {
    playNote(el.dataset.pitch);
  });
  el.addEventListener("mouseup", function() {
    stopNote();
  });
});

function handlePartialSettings(el) {
  var id = el.className.slice(-1);
  var val = el.value;
  switch (id) {
    case "1":
      synth.partial1 = val;
      synth.osc1.frequency.value = synth.baseFrequency * synth.partial1;
      break;
    case "2":
      synth.partial2 = val;
      synth.osc1.frequency.value = synth.baseFrequency * synth.partial2;
      break;
    case "3":
      synth.partial3 = val;
      synth.osc1.frequency.value = synth.baseFrequency * synth.partial3;
      break;
  }
}

window.onload = syncView();
