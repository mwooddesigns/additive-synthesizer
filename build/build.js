var buildify = require('buildify');

buildify().concat(['src/strict.js', 'src/additiveSynth.js', 'src/main.js'])
  .save('dist/master.js')
  .uglify()
  .save('dist/master.min.js');
